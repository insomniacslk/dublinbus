from distutils.core import setup

setup(
    name='dublinbus',
    version='1.0',
    author='Andrea Barberio',
    author_email='insomniac@slackware.it',
    description='A client for the DublinBus RTPI API',
    url='https://bitbucket.org/insomniacslk/dublinbus',
    packages=['dublinbus'],
)
