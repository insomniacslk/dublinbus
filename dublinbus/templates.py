# See http://rtpi.dublinbus.ie/DublinBusRoutePlannerService.asmx
# and http://rtpi.dublinbus.ie/DublinBusRTPIService.asmx for API

# TODO GET_DESTINATIONS
# TODO GET_DESTINATIONS_BY_ROUTE
# TODO GET_DESTINATIONS_BY_ROUTE_WITHIN_RANGE_OF_STOP_NUMBER
# TODO GET_ROUTE_PLANS
# TODO GET_ROUTE_PLANS_SINGLE_OPTION
# TODO SYNCHRONISE_SPATIAL_DATA
# TODO TEST_SERVICE

GET_DESTINATION_BY_STOP_NUMBER = '''<?xml version="1.0" encoding="utf-8"?>
<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
  <soap12:Body>
    <GetDestinationByStopNumber xmlns="http://dublinbus.ie/">
      <stopNumber>{stop_num}</stopNumber>
    </GetDestinationByStopNumber>
  </soap12:Body>
</soap12:Envelope>'''

GET_ALL_DESTINATIONS = '''<?xml version="1.0" encoding="utf-8"?>
<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
  <soap12:Body>
    <GetAllDestinations xmlns="http://dublinbus.ie/" />
  </soap12:Body>
</soap12:Envelope>'''

GET_PLACES = '''<?xml version="1.0" encoding="utf-8"?>
<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
  <soap12:Body>
    <GetPlaces xmlns="http://dublinbus.ie/">
      <filter>{filter}</filter>
    </GetPlaces>
  </soap12:Body>
</soap12:Envelope>'''

GET_ROUTES = '''<?xml version="1.0" encoding="utf-8"?>
<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
  <soap12:Body>
    <GetRoutes xmlns="http://dublinbus.ie/">
      <filter>{filter}</filter>
    </GetRoutes>
  </soap12:Body>
</soap12:Envelope>'''

GET_ROUTES_SERVICED_BY_STOP_NUMBER = '''<?xml version="1.0" encoding="utf-8"?>
<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
  <soap12:Body>
    <GetRoutesServicedByStopNumber xmlns="http://dublinbus.ie/">
      <stopId>{stop_num}</stopId>
    </GetRoutesServicedByStopNumber>
  </soap12:Body>
</soap12:Envelope>'''

GET_REAL_TIME_STOP_DATA = '''<?xml version="1.0" encoding="utf-8"?>
<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
  <soap12:Body>
    <GetRealTimeStopData xmlns="http://dublinbus.ie/">
      <stopId>{stop_num}</stopId>
      <forceRefresh>{refresh}</forceRefresh>
    </GetRealTimeStopData>
  </soap12:Body>
</soap12:Envelope>'''
