from __future__ import absolute_import
from __future__ import print_function

import re
import xml.etree.ElementTree as ET

import requests

from . import templates


DEBUG = False

API_ROUTEPLANNER = 'http://rtpi.dublinbus.ie/DublinBusRoutePlannerService.asmx'
API_RTPI = 'http://rtpi.dublinbus.ie/DublinBusRTPIService.asmx'

CHARSET = 'utf-8'
DEFAULT_REQUEST_HEADERS = {
    'Content-Type': 'application/soap+xml; charset={c}'.format(c=CHARSET),
}


rx_leading_curly_braces = re.compile(r'''^({[^}].+})?(?P<key>.+)$''')


def prettify(elem):
    """Return a pretty-printed XML string for the Element.
    """
    import xml.dom.minidom
    rough_string = ET.tostring(elem, 'utf-8')
    reparsed = xml.dom.minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="\t")


def remove_leading_curly_braces(text):
    '''
    Remove the leading curly braces and their content from a string. This is
    used for example to transform a string like
    "{http://rtpi.dublinbus.ie/}SomeField" into "SomeField"
    If there are no curly braces like that, return the string unmodified
    '''
    match = rx_leading_curly_braces.match(text)
    if match:
        return match.groupdict()['key']
    return text


def soap_request(operation, xml_request, api=API_RTPI):
    '''
    Make a SOAP request to the RTPI API and return the response XML as
    xml.etree.ElementTree.Element object.
    '''
    response = requests.post(
        api,
        params={'op': operation},
        data=xml_request,
        headers=DEFAULT_REQUEST_HEADERS,
    )
    text = response.text
    response.close()
    tree = ET.fromstring(text.encode(CHARSET))
    if DEBUG:
        print(prettify(tree))
    return tree


def _node_to_list(node):
    '''
    Internal function.
    Used to extract a list of children from an XML node as a list of
    dictionaries where keys and values are the children's sub-tags and
    respective values. This function doesn't recurse further and is not suited
    for XML-to-JSON conversion attempts.
    '''
    ret = []
    for subnode in node:
        dest = {}
        for field in subnode:
            key = remove_leading_curly_braces(field.tag)
            dest[key] = field.text
        ret.append(dest)
    return ret


def get_destination_by_stop_number(stop_num):
    '''
    API call GetDestinationByStopNumber
    '''
    xml_body = templates.GET_DESTINATION_BY_STOP_NUMBER.format(
        stop_num=stop_num)
    tree = soap_request('GetDestinationByStopNumber', xml_body,
                        api=API_ROUTEPLANNER)
    return _node_to_list(tree[0][0][0][0])


def get_routes_serviced_by_stop_number(stop_num):
    '''
    API call GetRoutesServicedByStopNumber
    '''
    xml_body = templates.GET_ROUTES_SERVICED_BY_STOP_NUMBER.format(
        stop_num=stop_num)
    tree = soap_request('GetRoutesServicedByStopNumber', xml_body,
                        api=API_RTPI)
    return _node_to_list(tree[0][0][0])


def get_all_destinations():
    '''
    API call GetAllDestinations
    '''
    xml_body = templates.GET_ALL_DESTINATIONS
    tree = soap_request('GetAllDestinations', xml_body, api=API_RTPI)
    return _node_to_list(tree[0][0][0][0])


def get_routes(searchterm):
    '''
    API call GetRoutes
    '''
    xml_body = templates.GET_ROUTES.format(filter=searchterm)
    tree = soap_request('GetRoutes', xml_body, api=API_RTPI)
    return _node_to_list(tree[0][0][0][0])


def get_places(searchterm):
    '''
    API call GetPlaces
    '''
    xml_body = templates.GET_PLACES.format(filter=searchterm)
    tree = soap_request('GetPlaces', xml_body, api=API_ROUTEPLANNER)
    return _node_to_list(tree[0][0][0][0])


def get_real_time_stop_data(stop_num, refresh=False):
    '''
    API call GetRealTimeStopData
    '''
    xml_body = templates.GET_REAL_TIME_STOP_DATA.format(
        stop_num=stop_num, refresh=str(bool(refresh)).lower())
    tree = soap_request('GetRealTimeStopData_ForceLineNoteVisit',
                        xml_body, api=API_RTPI)
    try:
        return _node_to_list(tree[0][0][0][1][0])
    except IndexError:
        return []
