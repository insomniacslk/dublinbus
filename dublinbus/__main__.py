from __future__ import absolute_import
from __future__ import print_function

import json
import argparse

from . import api


def parse_args():
    parser = argparse.ArgumentParser(prog='dublinbus')
    parser.add_argument('--debug', action='store_true', default=False,
                       help='Enable debug mode')
    parser.add_argument('--get-all-destinations', type=argparse.FileType('w'),
                        help='Get all the destinations, and save them to file '
                             'as JSON')
    parser.add_argument('--get-destination-by-stop', type=int,
                        help='Get destination by stop number')
    parser.add_argument('--get-routes-by-stop', type=int,
                        help='Get routes by stop number')
    parser.add_argument('--get-routes',
                        help='Get routes matching a search term, e.g. "77"')
    parser.add_argument('--get-places',
                        help='Get places matching a search term, e.g. '
                             '"trinity"')
    parser.add_argument('--get-real-time-stop-data',
                        help='Get real-time stop data. Needs the stop number')
    return parser.parse_args()


def main():
    args = parse_args()

    if args.debug:
        api.DEBUG = True

    if args.get_all_destinations:
        destinations = api.get_all_destinations()
        json.dump(destinations, args.get_all_destinations)
        print('## Saved all destinations to {filename}'.format(
            filename=args.get_all_destinations.name),
        )

    if args.get_destination_by_stop:
        stopnum = args.get_destination_by_stop
        print('## Destination for stop {stopnum}'.format(
            stopnum=stopnum),
        )
        print(json.dumps(api.get_destination_by_stop_number(stopnum), indent=4))

    if args.get_routes_by_stop:
        stopnum = args.get_routes_by_stop
        print('## Routes for stop {stopnum}'.format(
            stopnum=stopnum)
        )
        print(json.dumps(
            api.get_routes_serviced_by_stop_number(stopnum),
            indent=4)
        )

    if args.get_routes:
        searchterm = args.get_routes
        print('## Routes matching {st!r}'.format(st=searchterm))
        print(json.dumps(api.get_routes(searchterm), indent=4))

    if args.get_places:
        searchterm = args.get_places
        print('## Places matching {st!r}'.format(st=searchterm))
        print(json.dumps(api.get_places(searchterm), indent=4))

    if args.get_real_time_stop_data:
        stopnum = args.get_real_time_stop_data
        print('## Real-time data for stop {st!r}'.format(st=stopnum))
        print(json.dumps(api.get_real_time_stop_data(stopnum), indent=4))


if __name__ == '__main__':
    main()
