# README #


## What is it? ##

This is a Python client for the DublinBus [RTPI](http://www.dublinbus.ie/RTPI/)
(Real-Time Passenger Information) service. This service provides information
about the public transport in Dublin, IE.
The Python client provides a simple wrapper around the API. This module
currently provides:

* a Python library to use the RTPI API
* a command-line tool to provide higher level information

It aims to be a base for other services. E.g. I use it to notify myself the
upcoming buses, when I need them, based on my GPS location on my phone.

## How to install ##

In brief:

```bash
python setup.py install ## --user for local installation
usage: dublinbus [-h] [--debug] [--get-all-destinations GET_ALL_DESTINATIONS]
                 [--get-destination-by-stop GET_DESTINATION_BY_STOP]
                 [--get-routes-by-stop GET_ROUTES_BY_STOP]
                 [--get-routes GET_ROUTES] [--get-places GET_PLACES]
                 [--get-real-time-stop-data GET_REAL_TIME_STOP_DATA]

optional arguments:
  -h, --help            show this help message and exit
  --debug               Enable debug mode
  --get-all-destinations GET_ALL_DESTINATIONS
                        Get all the destinations, and save them to file as
                        JSON
  --get-destination-by-stop GET_DESTINATION_BY_STOP
                        Get destination by stop number
  --get-routes-by-stop GET_ROUTES_BY_STOP
                        Get routes by stop number
  --get-routes GET_ROUTES
                        Get routes matching a search term, e.g. "77"
  --get-places GET_PLACES
                        Get places matching a search term, e.g. "trinity"
  --get-real-time-stop-data GET_REAL_TIME_STOP_DATA
                        Get real-time stop data. Needs the stop number
```

## How to use it ##

### Show all the buses at the stop 123 ###

```bash
python -m dublinbus --get-real-time-stop-data 340
## Real-time data for stop '340'
[
    {
        "ServiceDelivery_Status": "true", 
        "MonitoredCall_ExpectedArrivalTime": "2015-10-17T18:22:02+01:00", 
        "MonitoredVehicleJourney_DirectionRef": "Inbound", 
        "MonitoredCall_VisitNumber": "64", 
        "StopMonitoringDelivery_RequestMessageRef": null, 
        "MonitoredVehicleJourney_LineRef": "27", 
        "MonitoredVehicleJourney_PublishedLineName": "77A", 
        "FramedVehicleJourneyRef_DatedVehicleJourneyRef": "12359", 
        "ServiceDelivery_ProducerRef": "bac", 
        "Timestamp": "2015-10-17T18:19:25.76+01:00", 
        "ServiceDelivery_MoreData": "false", 
        "MonitoredVehicleJourney_VehicleRef": "38005", 
        "ServiceDelivery_ResponseTimestamp": "2015-10-17T18:19:25.057+01:00", 
        "MonitoredCall_VehicleAtStop": "false", 
        "MonitoredCall_AimedDepartureTime": "2015-10-17T18:16:12+01:00", 
        "MonitoredCall_ExpectedDepartureTime": "2015-10-17T18:22:02+01:00", 
        "MonitoredCall_AimedArrivalTime": "2015-10-17T18:16:12+01:00", 
        "MonitoredStopVisit_MonitoringRef": "00340", 
        "StopMonitoringDelivery_Version": "1.0", 
        "MonitoredVehicleJourney_InCongestion": "false", 
        "MonitoredVehicleJourney_DestinationName": "Ringsend Rd via Tymon Park", 
        "StopMonitoringDelivery_ResponseTimestamp": "2015-10-17T18:19:25.057+01:00", 
        "MonitoredVehicleJourney_Monitored": "true", 
        "MonitoredVehicleJourney_BlockRef": "27009", 
        "MonitoredStopVisit_RecordedAtTime": "2015-10-17T18:19:25.057+01:00", 
        "FramedVehicleJourneyRef_DataFrameRef": "2015-10-17", 
        "MonitoredVehicleJourney_OperatorRef": "bac", 
        "MonitoredVehicleJourney_DestinationRef": "00354"
    }, 
...
]
```

### Same as above, but in Python code ###

```python
import dublinbus
dublinbus.api.get_real_time_stop_data(340)  # returns a list like in the above example
```
